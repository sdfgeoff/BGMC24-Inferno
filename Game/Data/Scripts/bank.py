class Bank:
    '''Stores cash - er, or whatever.

    Takes the initial amount when initilizing as well as optionally a min or
    max amount (cap) that can be deposited. The cap can be None in which case
    the bank can accept infinite money (well, 10^300 or so if it's a float)

    All transactions should be done with the transaction method, which will
    return a tuple of the amount deposited/withdrawn and the remainder.

    Check how much it has using the balance property

    You can also set caps on the minimum and maximum.
    '''
    def __init__(self, initial_reserve, min_cap=None, max_cap=None):
        self._amt = initial_reserve
        self.min_cap = min_cap
        self.max_cap = max_cap

        # Callbacks for onchange and onempty
        self.onchange = list()
        self.onempty = list()
        self.onfull = list()

    def transaction(self, amt):
        '''WIthdraw/deposit the specified amount. Returns how much was
        sucessfully deposited and the leftover'''
        if amt == 0:
            return (0.0, 0.0)

        self._amt += amt

        for funct in self.onchange:
            funct(self, amt)
        if self.max_cap is not None and self.balance >= self.max_cap:
            remainder = self.max_cap - self.balance
            self._amt = self.max_cap
            for funct in self.onfull:
                funct(self)
            return (remainder + amt, remainder)

        if self.min_cap is not None and self.balance <= self.min_cap:
            remainder = self.min_cap - self.balance
            self._amt = self.min_cap
            for funct in self.onempty:
                funct(self)
            return (remainder + amt, remainder)

        return (amt, 0.0)

    def check_at_least(self, amt):
        '''Checks to see if an addition will succeed'''
        return self.balance < amt

    @property
    def balance(self):
        '''Read only balance'''
        return self._amt

    def __float__(self):
        return float(self.balance)

    def __int__(self):
        return int(self.balance)


def some_tests():
    '''run some tests'''
    test_bank = Bank(100)
    assert test_bank.transaction(50) == (50, 0)
    assert test_bank.balance == 150
    test_bank.transaction(-100)
    assert test_bank.balance == 50
    test_bank.min_cap = 0
    assert test_bank.transaction(-75) == (-50, 25)
    assert test_bank.balance == 0

    test_bank.max_cap = 100
    assert test_bank.transaction(125) == (100, -25)
    assert test_bank.balance == 100


if __name__ == '__main__':
    some_tests()
