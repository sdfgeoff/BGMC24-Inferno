'''Commonly used fuctions'''
import os
import bge
import mathutils

import defs


def open_from_root(path, root_id=defs.ROOT_ID):
    '''Looks for a path relative to some ID file'''
    project_path = os.path.dirname(os.path.os.path.realpath(__file__))
    while root_id not in os.listdir(project_path):
        project_path = os.path.split(project_path)[0]
        if len(project_path) < 4:
            log.error("Unable to find root directory")
            break
    
    return os.path.join(project_path, path)


def get_mouse_over(scene):
    '''Returns the object the mouse is over assuming an orthographic scene
    with the camera at x=0, y=0'''
    pos = mathutils.Vector(bge.logic.mouse.position)

    # Coordinate mashing:
    pos -= mathutils.Vector([0.5, 0.5])
    pos.x *= scene.active_camera.ortho_scale
    height = bge.render.getWindowHeight()
    width = bge.render.getWindowWidth()
    pos.y *= -scene.active_camera.ortho_scale * height / width

    # Turn it into a vector
    start_pos = pos.to_3d() + mathutils.Vector([0, 0, 3.0])
    end_pos = start_pos - mathutils.Vector([0, 0, 1.0])
    hit, _, __ = scene.active_camera.rayCast(end_pos, start_pos, 40)
    return hit


if __name__ == "__main__":
    print(open_from_root(""))
