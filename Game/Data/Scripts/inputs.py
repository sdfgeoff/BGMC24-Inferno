import bge
import mathutils

import settings


def get_mouse_delta():
    '''Returns how much the mouse has moved this frame. Note: centers the mouse
    '''
    pos = mathutils.Vector(bge.logic.mouse.position)
    if pos[0] <= 0.0 or pos[1] <= 0.0:
        return mathutils.Vector([0, 0])
    bge.logic.mouse.position = (0.5, 0.5)

    # Normalize so the center is 0
    pos = mathutils.Vector([0.5, 0.5]) - pos

    # Remove artefact of screen aspect ratio
    pos.x *= bge.render.getWindowWidth() / bge.render.getWindowHeight()

    # Clamp if the motion is small
    if abs(pos.x) < settings.get_setting('mouse_deadzone'):
        pos.x = 0
    if abs(pos.y) < settings.get_setting('mouse_deadzone'):
        pos.y = 0

    # Invert mouse directions:
    if settings.get_setting('invert_mouse_y'):
        pos.y *= -1

    if settings.get_setting('invert_mouse_x'):
        pos.x *= -1

    # Mouse sensitiity
    pos.x *= settings.get_setting('mouse_sensitivity_x')
    pos.y *= settings.get_setting('mouse_sensitivity_y')

    return pos


def center_mouse():
    '''Dumps the mouse cursor into the middle of the screen'''
    bge.logic.mouse.position = (0.5, 0.5)
