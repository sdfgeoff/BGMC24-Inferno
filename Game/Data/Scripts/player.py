import math

import bge
import mathutils

import inputs
import bank
import defs


def init(cont):
    '''Creates a player object - used for testing only'''
    def die(_player):
        '''Displays something when the player dies'''
        print("Player is DEAD")

    cont.owner['PLAYER'] = Player(cont.owner)
    cont.owner['PLAYER'].shield.onempty.append(die)
    cont.script = __name__ + '.run'


def run(cont):
    '''Updates the player object - in testing only'''
    cont.owner['PLAYER'].update()


class Player(object):
    '''Represents the player controlled vehilcle'''
    def __init__(self, spawn_obj):
        self.obj = spawn_obj.scene.addObject('PlayerPhysics')
        self.obj.worldTransform = spawn_obj.worldTransform
        self.velocity = 6.0  # ~bu/s
        self.boost = bank.Bank(
            0,
            0,
            defs.PLAYER_BOOST_SECONDS
        )
        self.slow = bank.Bank(
            0,
            0,
            defs.PLAYER_SLOW_SECONDS
        )
        self.shield = bank.Bank(
            0,
            0,
            defs.PLAYER_SHIELD_SECONDS
        )

        self._hit_list = list()
        self.obj.collisionCallbacks.append(self._on_hit)

        self.obj.scene.active_camera = self.obj.children['PlayerCamera']

        self.last_interface = 0
        self.level = None
        self.distance = 0.0
        self.speed = 0.0
        self.min_shield = defs.PLAYER_SHIELD_SECONDS

    def update(self, timestep=1/60):
        '''Handles link between player's input and actions'''
        boost = bge.events.LEFTMOUSE in bge.logic.mouse.active_events
        slow = bge.events.RIGHTMOUSE in bge.logic.mouse.active_events

        if not boost:
            charge = defs.PLAYER_BOOST_SECONDS / defs.PLAYER_BOOST_REGEN_SECONDS
            self.boost.transaction(charge * timestep)

        if not slow:
            charge = defs.PLAYER_SLOW_SECONDS / defs.PLAYER_SLOW_REGEN_SECONDS
            self.slow.transaction(charge * timestep)

        self.shield.transaction(
            1 / defs.PLAYER_SHIELD_REGEN_SECONDS * timestep
        )

        boost_amt, _ = self.boost.transaction(-timestep * boost)
        boost = -boost_amt / timestep  # How much boost succeeded in percent
        if not boost:
            # If the player isn't boosting, slowly increase speed.....
            # I'm evil like that....
            self.velocity += timestep / defs.SPEED_UP_TIME
        boost = boost * (defs.PLAYER_BOOST_FACTOR - 1) + 1  # Scaled properly

        slow_amt, _ = self.slow.transaction(-timestep * slow)
        slow = -slow_amt / timestep  # How much boost succeeded in percent
        slow = slow * (defs.PLAYER_SLOW_FACTOR - 1) + 1  # Scaled properly

        self._update_angular(inputs.get_mouse_delta(), slow, timestep)
        self._update_linear(boost, slow, timestep)

        self.distance += self.obj.localLinearVelocity.length / 100
        self.speed = self.obj.localLinearVelocity.y * 10

        self.min_shield = min(self.shield.balance, self.min_shield)

        if self._hit_list:
            for _obj, _pos, hit_normal in self._hit_list:
                bounce = -hit_normal * defs.PLAYER_BOUNCE_FORCE
                self.obj.applyForce(bounce * timestep, False)
            self._hit_list = list()
            self.shield.transaction(-timestep)

    def _update_linear(self, boost, slow, timestep):
        '''Handles player speed and motion'''
        force_vec = mathutils.Vector([0, 0, 0])

        # From the engines
        delta = boost * slow

        force_vec.y += self.velocity * delta * defs.PLAYER_LINEAR_DAMPING_FACTOR

        # From drag
        lin_vel = self.obj.localLinearVelocity
        force_vec -= lin_vel * defs.PLAYER_LINEAR_DAMPING_FACTOR

        # Enact the forces
        self.obj.applyForce(force_vec * timestep, True)

    def _update_angular(self, input_vec, _slow, timestep):
        '''Rotates the player'''
        torque_vec = mathutils.Vector([0, 0, 0])

        # Torque from input:
        delta = input_vec
        torque_vec += mathutils.Vector([delta.y, 0.0, delta.x])
        torque_vec.y -= delta.x * defs.PLAYER_ROLL_FACTOR
        # So the ui sensitivity is ~0-100 we have an extra factor:
        torque_vec *= defs.PLAYER_SENSITIVITY_FACTOR  # * slow

        # Torque from damping:
        ang_vel = self.obj.localAngularVelocity
        torque_vec -= ang_vel * defs.PLAYER_ANGULAR_DAMPING_FACTOR

        # Torque from auto-leveling
        floor_normal = self.get_floor_normal()
        if floor_normal is not None:
            up_axis = self.obj.getAxisVect([0, 0, 1.0])
            rot_diff_global = floor_normal.cross(up_axis)
            ori_inverted = self.obj.worldOrientation.inverted()
            rot_diff_local = ori_inverted * rot_diff_global
            torque_vec.y -= rot_diff_local.y * defs.PLAYER_AUTOLEVELING_FACTOR

        # Enact the torques
        self.obj.applyTorque(torque_vec * timestep, True)

    def get_floor_normal(self):
        # Cast ray down to floor and get normal
        hit_normal = None
        counter = 0
        search_angle = 0
        while hit_normal is None and counter < defs.PLAYER_MAX_FLOOR_RAYS:
            counter += 1  # Prevent lockup

            ray_start = self.obj.worldPosition
            local_direction = [
                math.sin(search_angle),
                0,
                -math.cos(search_angle)
            ]
            ray_end = ray_start + self.obj.getAxisVect(local_direction)
            _hit, _pos, hit_normal = self.obj.rayCast(
                ray_end,
                ray_start,
                10,
                defs.GROUND_PROPERTY,
                1,  # Return face normal
                1,  # X-ray
            )
            # Search further away than down
            search_angle += (counter % 2) * math.pi / defs.PLAYER_MAX_FLOOR_RAYS
            search_angle *= -1

        if hit_normal is not None:
            return hit_normal
        return None

    def _on_hit(self, *args):
        '''Runs when the player hit's something.

        We just store that it hit something - this prevents multiple hits in
        the same frame from affecting the player many times'''
        if 'INTERFACE' not in args[0]:
            self._hit_list.append(args)
        else:
            offset = args[0]['OFFSET']
            if self.level is not None:
                self.level.set_player_at(offset)

            if not offset >= self.last_interface:
                # WRONG WAY!!!!!
                self.shield.transaction(-1/10)
            self.last_interface = offset
