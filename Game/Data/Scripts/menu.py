import time
import bge

import settings
import inputs
import common

MENU_TEXT = '''   MUSIC  [{}]  [{}]  POSTFX
SOUNDS  [{}]  [{}]  SHADOWS

Mouse Sensitivity:
X:  <  {}  >        Y:  <  {}  >

Mouse Invert  [{}]

[ RESUME ]           [ QUIT ]'''


def init(cont):
    '''Sets up the menu'''
    cont.owner['MENU'] = Menu(cont.owner.scene)
    cont.script = __name__ + '.update'


def update(cont):
    '''Updates the menu'''
    cont.owner['MENU'].update()


class Menu(object):
    '''The options menu'''
    def __init__(self, scene):
        self.scene = scene
        self.text_obj = scene.objects['MainText']
        self.text_obj.resolution += 2
        bge.render.showMouse(True)
        self.redraw_text()

        self.last_click_time = 0

        self.action_dict = {
            'QuitButton': self._quit,
            'ResumeButton': self._resume,
            'shadows': self._bool_button,
            'postfx': self._bool_button,
            'sounds': self._bool_button,
            'music': self._bool_button,
            '-mouse_sensitivity_x': self._int_button,
            '+mouse_sensitivity_x': self._int_button,
            '-mouse_sensitivity_y': self._int_button,
            '+mouse_sensitivity_y': self._int_button,
            'invert_mouse_y': self._bool_button,
        }

        for sce in bge.logic.getSceneList():
            if sce is not scene:
                sce.suspend()

    def redraw_text(self):
        '''Draws all of the text seen by the user'''
        self.text_obj.text = MENU_TEXT.format(
            disp_bool(settings.get_setting('music')),
            disp_bool(settings.get_setting('postfx')),
            disp_bool(settings.get_setting('sounds')),
            disp_bool(settings.get_setting('shadows')),
            settings.get_setting('mouse_sensitivity_x'),
            settings.get_setting('mouse_sensitivity_y'),
            disp_bool(settings.get_setting('invert_mouse_y')),
        )

    def update(self):
        '''Checks to see what's been clicked on'''
        click_status = bge.logic.mouse.events[bge.events.LEFTMOUSE]
        if click_status == bge.logic.KX_INPUT_ACTIVE:
            if time.time() - self.last_click_time < 0.1:
                return
            self.last_click_time = time.time()
            hit = common.get_mouse_over(self.scene)

            if hit is not None and hit.name in self.action_dict:
                self.action_dict[hit.name](hit.name)

    @staticmethod
    def _quit(_name):
        '''Quits the game'''
        bge.logic.endGame()

    def _resume(self, _name):
        '''Jumps back into the game'''
        for scene in bge.logic.getSceneList():
            scene.resume()
        bge.render.showMouse(False)
        inputs.center_mouse()
        self.scene.end()

    def _bool_button(self, name):
        '''Toggles the specified setting'''
        settings.set_setting(name, not settings.get_setting(name))
        self.redraw_text()

    def _int_button(self, name):
        '''Changes the value of a setting associated with an integer'''
        direction = int(((name[0] == '+')-0.5) * 2)
        name = name[1:]
        settings.set_setting(name, direction + settings.get_setting(name))
        self.redraw_text()


def disp_bool(val):
    '''Converts a boolean into something to display'''
    if val:
        return '~'
    return '  '
