import random
import os

import bge
import settings
import common


class SuperFilter(object):
    '''Note that cont MUST have a 2D filter actuator'''
    def __init__(self, cont, player):
        self.cont = cont

        filter_path = common.open_from_root('Data/Textures/filter.frag')
        self.filter_text = open(filter_path).read()

        self.player = player
        self.obj = cont.owner

        self.obj['chromatic_abbr'] = 0.01
        self.obj['zoom_amt'] = 1.0
        self.obj['fisheye_amt'] = 2.0
        self.obj['streak_length'] = 3.0
        self.obj['streak_center_x'] = 0.5
        self.obj['streak_center_y'] = 0.5

        self.actuator = cont.actuators['Filter']
        self.actuator.shaderText = self.filter_text

        if settings.get_setting('postfx'):
            cont.activate(self.actuator)

        self._enable_changed = False
        settings.set_callback('postfx', self._enable_filter)

    def _enable_filter(self, _prop, _value):
        '''We need the controller to access the 2D filters, so we just store
        that it has changed so the filter can be updated on the next update
        pass'''
        self._enable_changed = True

    def update(self, cont):
        '''Updates the filter with data from the player'''
        center_x, center_y = self.player.obj.localLinearVelocity.xz / 10.0
        self.obj['streak_center_x'] = center_x + 0.5
        self.obj['streak_center_y'] = center_y + 0.5

        self.obj['streak_length'] = self.player.obj.localLinearVelocity.y / 5.0
        self.obj['streak_offset'] = random.random()

        if self._enable_changed:
            self._enable_changed = False
            new_val = settings.get_setting('postfx')
            self.set_enabled(cont, new_val)

    def set_enabled(self, cont, mode):
        '''Sets if the filter is currently in use or not'''
        if mode:
            self.actuator.mode = bge.logic.RAS_2DFILTER_CUSTOMFILTER
            self.actuator.shaderText = self.filter_text
            cont.activate(self.actuator)
        else:
            self.actuator.mode = bge.logic.RAS_2DFILTER_DISABLED
            cont.activate(self.actuator)
