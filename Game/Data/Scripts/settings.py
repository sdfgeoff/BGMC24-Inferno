import json
import bge
import os

import common

SETTINGS = {
    'invert_mouse_x': False,
    'invert_mouse_y': False,
    'mouse_deadzone': 0.002,
    'mouse_sensitivity_x': 50,
    'mouse_sensitivity_y': 50,
    'music':True,
    'sounds':True,
    'postfx':True,
    'shadows':True
}

CALLBACKS = dict()

def get_setting(name):
    '''Returns the value of a setting'''
    return SETTINGS[name]


def set_callback(name, function):
    '''Registers a function to be run when a value changes'''
    CALLBACKS[name] = function
    function(name, get_setting(name))

def set_setting(name, val):
    SETTINGS[name] = val
    if name in CALLBACKS:
        CALLBACKS[name](name, get_setting(name))
    save_settings()


def save_settings():
    json.dump(SETTINGS, open(common.open_from_root('settings.conf'), 'w'), indent=2)


def load_settings():
    try:
        new = json.load(open(common.open_from_root('settings.conf'), 'r'))
        SETTINGS.update(new)
    except:
        save_settings()


load_settings()
