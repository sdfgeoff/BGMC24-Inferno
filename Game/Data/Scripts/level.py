import math


NUM_SEGMENTS = 70


def init(cont):
    '''Creates a level fragment used for testing'''
    cont.owner['LEVEL'] = LevelSegment(1980, cont.owner)
    cont.script = __name__ + '.update'


def update(cont):
    '''Forces the level on to the next segment'''
    cont.owner['LEVEL'].go_to_next_segment()


class LevelSegment(object):
    '''Represents a level segment from a specific fork in the track'''
    def __init__(self, seed, scene):
        self.scene = scene
        self.start_room = scene.objects['STARTROOM']
        self.tiles = {0: self.start_room}
        self.rand_gen = RandomNumberGenerator(seed)
        self.offset = 1

        for i in range(int(NUM_SEGMENTS / 2)):
            self._add_tile(i+1)

    def _add_tile(self, offset):
        '''Addss the tile - so long as one of it's neighbours already exists'''
        if offset in self.tiles:
            return

        last_tile = self.tiles[offset - 1]
        last_exit = get_exits(last_tile)[0]
        tile_list = get_tiles_with_interface(self.scene, last_exit['interface'])
        tile = rand_float_to_choice(self.rand_gen.get(offset), tile_list)
        added_tile = self.scene.addObject(tile)
        added_interface = self.scene.addObject('Interface')
        added_interface.setParent(added_tile)
        added_interface.localOrientation = [0, 0, 0]
        added_interface.localPosition = [0, 0, 0]
        added_interface['OFFSET'] = offset

        pos = last_exit.worldPosition
        ori = last_exit.worldOrientation.to_euler()
        added_tile.worldPosition = pos
        added_tile.worldOrientation = ori

        self.tiles[offset] = added_tile

    def _remove_tile(self, offset):
        tile = self.tiles[offset]
        if tile != self.start_room:
            tile.endObject()
            del self.tiles[offset]

    def set_player_at(self, pos):
        '''Tells the level where the player currently is in the level'''
        min_id = max(0, pos - int(NUM_SEGMENTS / 2))
        max_id = pos + int(NUM_SEGMENTS / 2)
        while max_id not in self.tiles:
            self._add_tile(self.get_biggest_tile_id() + 1)

        if min_id is not 0:
            while min_id in self.tiles:
                self._remove_tile(self.get_smallest_tile_id())

    def get_biggest_tile_id(self):
        '''Returns the offset of the frontmost tile'''
        keys = list(self.tiles.keys())
        return sorted(keys)[-1]

    def get_smallest_tile_id(self):
        '''Returns the offset of the furthest back tile'''
        keys = list(self.tiles.keys())
        return sorted(keys)[1]


def get_tiles_with_interface(scene, interface):
    '''Returns a list of all tiles with a speciic interface'''
    tiles = list()
    for obj in scene.objectsInactive:
        if obj.parent is None:  # Not interested in exits
            interface_str = obj.get('interface', '')
            if interface in interface_str.split(','):
                tiles.append(obj)
    return tiles


def get_exits(tile):
    '''Returns a list of exits associated with the tile'''
    return [c for c in tile.children if 'interface' in c]


class RandomNumberGenerator(object):
    '''Generates random numbers from a seed'''
    def __init__(self, seed):
        self.seed = seed

    def get(self, offset):
        '''returns the next random float'''
        import random
        random.seed(self.seed + offset)
        return random.random()  # For testing


def rand_float_to_choice(num, array):
    '''Choses from a list based on a float between 0 and 1'''
    index = math.floor(num * len(array))
    index = min(index, len(array) - 1)  # Catch edge case where num == 1
    index = max(index, 0)  # Safety
    return array[index]
