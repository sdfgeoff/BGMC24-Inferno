import bge
import defs
import common


class Achievements(object):
    '''Stores and checks what achievments the player has been awarded'''
    def __init__(self, player, hud):
        self.player = player
        self.hud = hud
        self.completed = list()

        bge.logic.globalDict['SCORES'] = self

        self._testers = {
            'distance': self._check_distance,
            'speed': self._check_speed,
            'pheonix': self._check_recovery,
        }

        self.score_bonus = 0.0
        self.score = 0.0
        self.tmp_speed_bonus = 0

    def update(self):
        '''Checks all the scores, fades out the bonus tracker etc.'''
        speed_bonus = max(0, (self.player.speed - 150) / 50)
        if speed_bonus > 0:
            self.tmp_speed_bonus += speed_bonus
            self.set_bonus_text(self.tmp_speed_bonus)
        else:
            self.score_bonus += self.tmp_speed_bonus
            self.tmp_speed_bonus = 0

        self.hud.score_bonus_text.color = self.hud.score_bonus_text.color * 0.99
        # Check Achievements
        for achievement in defs.ACHIEVEMENTS_LIST:
            if achievement not in self.completed:
                if self._testers[achievement['type']](achievement):
                    self.completed.append(achievement)
                    self.reached_achievement(achievement)

        self.hud.score_text.text = "  {:06d}".format(
            int(self.score + self.score_bonus)
        )
        self.score = self.player.distance


    def reached_achievement(self, achievement):
        '''Adds a bonus to the score'''
        self.hud.message_bar.add_message(achievement['name'])
        if self.tmp_speed_bonus != 0:
            # Because it is already showing
            self.tmp_speed_bonus += achievement['points']
        else:
            self.score_bonus += achievement['points']
            self.set_bonus_text(achievement['points'])

    def set_bonus_text(self, val):
        self.hud.score_bonus_text.text = "  {:+8d}".format(int(val))
        self.hud.score_bonus_text.color = [0, 1, 1, 1]

    def _check_distance(self, data):
        return self.player.distance > data['data']

    def _check_speed(self, data):
        return self.player.speed > data['data']

    def _check_recovery(self, data):
        min_shield_percent = self.player.min_shield / defs.PLAYER_SHIELD_SECONDS
        shield_percent = self.player.shield.balance / defs.PLAYER_SHIELD_SECONDS

        return min_shield_percent < data['data'][0] and shield_percent > data['data'][1]


def get_best_score(current_score):
    try:
        score = int(open(common.open_from_root(defs.SCORE_FILE)).readline())
    except:
        score = 0

    save_score(max(score, int(current_score)))
    return score

def save_score(val):
    '''Saves the specified score to a file'''
    try:
        open(common.open_from_root(defs.SCORE_FILE), 'w').write(str(val))
    except:
        pass

