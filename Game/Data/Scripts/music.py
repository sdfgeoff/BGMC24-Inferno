import random
import bge
import aud
import settings


DEVICE = aud.device()


class Music(object):
    '''The soundtrack for the game'''
    def __init__(self, cont):
        self.cont = cont
        self.act = cont.actuators['Music']
        cont.activate(self.act)
        if settings.get_setting('music'):
            self.act.volume = 1.0
        else:
            self.act.volume = 0.0

    def end_game(self):
        '''Winds down the music'''
        self.act.pitch *= 0.95

    def update(self):
        '''Updates the music volume'''
        if settings.get_setting('music'):
            self.act.volume = 1.0
        else:
            self.act.volume = 0.0


class Sounds(object):
    def __init__(self, cont, player):
        self.cont = cont
        self.player = player
        self.collide = cont.actuators['Collide']
        self.air = DEVICE.play(cont.actuators['Air'].sound.loop(-1))
        self.air.volume = 0.0
        self.booster = DEVICE.play(cont.actuators['Booster'].sound.loop(-1))
        self.booster.volume = 0.0
        self.booster_amt = 0.0

        self.hit = False
        player.obj.collisionCallbacks.append(self.on_hit)
        player.boost.onchange.append(self.booster_active)

    def on_hit(self, *args):
        '''Checks to see if the player has hit a wall and tells it to
        play the hit sound on the next frame. This was originally because
        I just activated the controller. Now it's to stop it happening
        too many times'''
        if 'INTERFACE' not in args[0]:
            self.hit = True

    def booster_active(self, _bank, amt):
        '''Tweaks the booster sound'''
        if amt < 0:
            target = 1.0
        else:
            target = 0.0
        if settings.get_setting('sounds'):
            self.booster_amt = target * 0.1 + self.booster_amt * 0.9
            self.booster.volume = (self.booster_amt ** 0.5) * 2.0
            self.booster.pitch = 3.0 - self.booster_amt * 2.0
        else:
            self.booster.volume = 0.0

    def update(self, _cont):
        '''Updates the sounds and adds collision ones if necessary'''
        if settings.get_setting('sounds'):
            velocity = self.player.obj.worldLinearVelocity.length
            self.air.volume = velocity / 8.0
            self.air.pitch = velocity / 12.0 + 0.1
        else:
            self.air.volume = 0.0

        if self.hit and settings.get_setting('sounds'):
            fac = self.collide.sound
            handle = DEVICE.play(fac)
            handle.pitch = random.random() + 0.05
            self.hit = False
