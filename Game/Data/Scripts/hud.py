import time
import math

import bge
import mathutils

import defs

bge.logic.addScene('HUD')


class HUD(object):
    '''The heads up display'''
    def __init__(self, player):
        self.player = player
        self.scene = [s for s in bge.logic.getSceneList() if s.name == 'HUD'][0]

        # Get all the various objects:
        self.direction_indicator = self.scene.objects['DirectionIndicator']
        self.left_indicator_a = self.scene.objects['LeftIndicatorA']
        self.left_indicator_b = self.scene.objects['LeftIndicatorB']
        self.right_indicator = self.scene.objects['RightIndicator']
        self.velocity_indicator = self.scene.objects['VelocityText']
        self.distance_indicator = self.scene.objects['DistanceText']
        self.score_text = self.scene.objects['ScoreText']
        self.score_bonus_text = self.scene.objects['ScoreBonusText']
        self.low_shield = self.scene.objects['LowShieldWarning']

        # Fix text resolutions
        self.velocity_indicator.resolution = 3
        self.distance_indicator.resolution = 3
        self.score_text.resolution = 3

        # Set default values:
        self.low_shield.visible = False
        self.left_indicator_a.color = [0.0] * 4
        self.left_indicator_b.color = [0.0] * 4
        self.right_indicator.color = [0.0] * 4

        # The spawn/death effect:
        self.mask = self.scene.objects['Death']
        self.mask.color = [0.0] * 4
        self.end_game = False

        # The bar at the top used to display achievements:
        self.message_bar = MessageBar(self.scene.objects['MessageBar'])

        # Configure callbacks:
        self.player.boost.onchange.append(self.boost)
        self.player.boost.onempty.append(self.boost_empty)
        self.player.slow.onchange.append(self.slow)
        self.player.slow.onempty.append(self.slow_empty)
        self.player.shield.onchange.append(self.shield)
        self.player.obj.collisionCallbacks.append(self.indicate_hit)

        # Collision direction indicators
        self.time_of_last_indicator = 0
        self.indicators = list()

    def update(self):
        '''Updates a couple of things on the HUD - the ones not easily done
        by callbacks - eg things that animate from python'''
        velocity = self.player.obj.localLinearVelocity.xz / 3.0
        self.direction_indicator.worldPosition.xy = velocity
        self.message_bar.update()

        for indicator in self.indicators[:]:
            if indicator.update():
                self.indicators.remove(indicator)

        if self.end_game:
            if self.mask.color.x > 0:
                self.mask.color -= mathutils.Vector([0.01] * 3 + [1])
        else:
            if self.mask.color.x < 1.0:
                self.mask.color += mathutils.Vector([0.03] * 3 + [1])

            self.velocity_indicator.text = "{:>7.1f}".format(self.player.speed)
            self.distance_indicator.text = "{:06.1f}".format(
                self.player.distance
            )

    def boost(self, _bank, _amt):
        '''Update the boost bar'''
        percent = _bank.balance / defs.PLAYER_BOOST_SECONDS
        color_value = min(0.95, 1.0 - percent)
        self.left_indicator_a.color = [color_value] * 3 + [1.0]

    def boost_empty(self, _bank):
        '''Runs when the boost drive is emtpy'''
        self.message_bar.add_message("Boost Drive Empty")

    def slow(self, _bank, _amt):
        '''Updates the time warp bar'''
        percent = _bank.balance / defs.PLAYER_SLOW_SECONDS
        color_value = min(0.95, 1.0 - percent)
        self.left_indicator_b.color = [color_value] * 3 + [1.0]

    def slow_empty(self, _empty):
        '''Runs when the time warp drive is empty'''
        self.message_bar.add_message("Time Warp Empty")

    def shield(self, _bank, _amt):
        '''Runs when the shield changes - updates the shield bar'''
        percent = _bank.balance / defs.PLAYER_SHIELD_SECONDS
        self.low_shield.visible = percent < 0.3
        color_value = min(0.95, 1.0 - percent)
        self.right_indicator.color = [color_value] * 3 + [1.0]

    def indicate_hit(self, hit_obj, hit_pos, _hit_normal):
        '''When the player is hit, add a red glow onto the HUD (indicator
        object)'''
        if 'INTERFACE' not in hit_obj and not self.end_game:
            if time.time() - self.time_of_last_indicator > 0.3:
                self.time_of_last_indicator = time.time()
                _dist, _glob, local_dir = self.player.obj.getVectTo(hit_pos)
                angle = math.atan2(-local_dir.z, -local_dir.x)
                self.indicators.append(Indicator(self.scene, angle))


class Indicator(object):
    '''The red glow that appears when the player hits something'''
    def __init__(self, scene, angle):
        self.indicator = scene.addObject('HitIndicators')
        self.indicator.worldOrientation = [0, 0, angle]
        self.time = time.time()

    def update(self):
        '''Fades the indicator out and deletes it if old enough'''
        self.indicator.color *= 0.9
        if self.indicator.color.length < 0.01:
            self.indicator.endObject()
            return True


class MessageBar(object):
    '''The text at the top of the screen that displays the coutdown and
    achievement names etc.'''
    def __init__(self, text_obj):
        self.obj = text_obj
        self.messages = list()
        self.obj.text = ''
        self.obj.resolution = 3

    def add_message(self, message):
        '''Adds a message onto the display'''
        for existing in self.messages:
            if existing[0] == message:
                return  # Don't add existing messages, or even update the time
        self.messages.append((message, time.time()))

    def update(self):
        '''Splats the most recent message onto the screen. Deletes old ones'''
        if self.messages:
            out_str = ''
            num_chars = len(self.messages[-1][0])

            # Center the text (approximately)
            center_count = math.ceil((22 - num_chars) * 1 + 0.5)
            out_str += ' ' * center_count + self.messages[-1][0]

            if self.messages[0][1] < time.time() - defs.HUD_MESSAGE_TIME:
                self.messages = self.messages[1:]

            self.obj.text = out_str
        else:
            self.obj.text = ''
