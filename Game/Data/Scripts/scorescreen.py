'''The screen used to display the results to the user'''
import bge

import score
import common


def init(cont):
    '''This is completely independant from the game'''
    cont.owner['ScoreScreen'] = ScoreScreen(cont.owner.scene)
    cont.script = __name__ + '.update'


def update(cont):
    '''Update the score screen'''
    cont.owner['ScoreScreen'].update()


class ScoreScreen(object):
    '''Handles displaying the score information to the user'''
    def __init__(self, scene):
        self.scene = scene

        # Grab the scores the last game
        if 'SCORES' in bge.logic.globalDict:
            self.scores = bge.logic.globalDict['SCORES']
        else:
            self.scores = score.Achievements(None, None)

        # Find and configure text objects
        self.score_text = self.scene.objects['ScoreEndText']
        self.achievement_text = self.scene.objects['AchievementsText']
        for obj in self.scene.objects:
            if isinstance(obj, bge.types.KX_FontObject):
                obj.resolution = 3

        # Show text to the user
        current_score = self.scores.score + self.scores.score_bonus
        self.score_text.text = 'Score:   {:06d}\nBest:      {:06d}'.format(
            int(current_score),
            int(score.get_best_score(current_score))
        )
        achievement_text = 'Achievements:\n\n'
        for id_num, achievement in enumerate(self.scores.completed):
            achievement_text += '{:>15}'.format(achievement['name'])
            if id_num % 2 == 1:
                achievement_text += '\n'

        self.achievement_text.text = achievement_text

    def update(self):
        '''Check to see if we need to go back to the game'''
        #Enable the mouse cursor
        bge.render.showMouse(True)

        # See what's clicked on
        click_status = bge.logic.mouse.events[bge.events.LEFTMOUSE]
        if click_status == bge.logic.KX_INPUT_ACTIVE:
            hit = common.get_mouse_over(self.scene)
            if hit is not None:
                if hit.name == 'options':
                    bge.logic.addScene('Menu')
                elif hit.name == 'new_game':
                    life_path = 'Data/Game/Game.blend'
                    bge.logic.startGame(common.open_from_root(life_path))
                elif hit.name == 'quit':
                    bge.logic.endGame()
