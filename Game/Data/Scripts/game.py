import time
import random
import os
import bge


import player
import settings
import level
import hud
import filter2d
import defs
import inputs
import music
import score

import common


def init(cont):
    '''Starts the game'''
    cont.owner['GAME'] = Game(cont.owner.scene)
    cont.script = __name__ + '.run'


def run(cont):
    '''Run the game'''
    cont.owner['GAME'].run(cont)


class Game(object):
    def __init__(self, scene):
        self.scene = scene
        self.run = self.load

        bge.render.setGLSLMaterialSetting('shadows', settings.get_setting('shadows'))
        settings.set_callback('shadows', self._update_shadows)

    def _update_shadows(self, name, val):
        bge.render.setGLSLMaterialSetting('shadows', settings.get_setting('shadows'))


    def load(self, cont):
        '''Load what needs loading'''
        if 'SEED' not in bge.logic.globalDict:
            seed = int(random.random() * 1000)
        else:
            seed = bge.logic.globalDict['SEED']

        self.level = level.LevelSegment(seed, self.scene)
        self.player = player.Player(self.scene.objects['STARTROOM'])
        self.player.level = self.level
        self.player.shield.onempty.append(self._end_game_callback)
        self.hud = hud.HUD(self.player)
        self.music = music.Music(cont)
        self.sounds = music.Sounds(cont, self.player)
        self.achievements = score.Achievements(self.player, self.hud)

        self.filter2d = filter2d.SuperFilter(cont, self.player)
        self.filter2d.update(cont)

        # So that the HUD updates:
        self.player.slow.transaction(0.01)
        self.player.boost.transaction(0.01)
        self.player.shield.transaction(0.01)

        self.hud.message_bar.add_message("Run from the Fire")

        self.time = time.time()
        self.death_time = None
        self.run = self.intro

    def intro(self, cont):
        '''Play pretty animations to get the player into the mood'''
        time_since_start = time.time() - self.time

        if time_since_start > 0.1:
            self.player.boost.transaction(defs.PLAYER_BOOST_SECONDS / 60)
        if time_since_start > 0.4:
            self.player.slow.transaction(defs.PLAYER_SLOW_SECONDS / 60)
        if time_since_start > 0.7:
            self.player.shield.transaction(defs.PLAYER_SHIELD_SECONDS / 120)

        self.hud.scene.objects['HelpTextTimeWarp'].visible = flicker(2.8 - time_since_start)
        self.hud.scene.objects['HelpTextBoost'].visible = flicker(3.0 - time_since_start)
        self.hud.scene.objects['HelpTextShield'].visible = flicker(3.5 - time_since_start)

        self.hud.low_shield.visible = False

        if time_since_start > 1.0 and time_since_start < 1.5:
            self.hud.message_bar.add_message("3")
        if time_since_start > 2.0 and time_since_start < 2.5:
            self.hud.message_bar.add_message("2")
        if time_since_start > 3.0 and time_since_start < 3.5:
            self.hud.message_bar.add_message("1")
        if time_since_start > 4.0 or not defs.ENABLE_INTRO:
            self.hud.message_bar.add_message("GO")
            self.run = self.update
            self.player.boost.transaction(defs.PLAYER_BOOST_SECONDS)
            self.player.slow.transaction(defs.PLAYER_SLOW_SECONDS)
            self.player.shield.transaction(defs.PLAYER_SHIELD_SECONDS)

            self.hud.scene.objects['HelpTextTimeWarp'].visible = False
            self.hud.scene.objects['HelpTextBoost'].visible = False
            self.hud.scene.objects['HelpTextShield'].visible = False

        inputs.center_mouse()

        self.hud.update()
        self.filter2d.update(cont)
        self.music.update()

        if bge.events.ESCKEY in bge.logic.keyboard.active_events:
            bge.logic.addScene('Menu')

    def _end_game_callback(self, *args):
        self.run = self.end_game
        self.hud.end_game = True
        if self.death_time is None:
            self.death_time = time.time()

    def end_game(self, cont):
        self.music.end_game()
        self.sounds.update(cont)
        self.hud.update()

        if time.time() - self.death_time > 2.0:
            death_path = 'Data/UI/Score.blend'
            bge.logic.startGame(common.open_from_root(death_path))
            #bge.logic.restartGame()

    def update(self, cont):
        self.player.update()
        self.filter2d.update(cont)
        self.hud.update()
        self.sounds.update(cont)
        self.music.update()
        self.achievements.update()

        if bge.events.ESCKEY in bge.logic.keyboard.active_events:
            bge.logic.addScene('Menu')


def flicker(probability):
    return random.random() < probability
