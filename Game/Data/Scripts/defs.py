'''This is for non-user configurable properties'''

ROOT_ID = 'README.txt'

GROUND_PROPERTY = 'GROUND'

PLAYER_SENSITIVITY_FACTOR = 60
PLAYER_LINEAR_DAMPING_FACTOR = 300  # Essentially the slidyness sideways
PLAYER_ANGULAR_DAMPING_FACTOR = 30  # How long turning takes to vanish
PLAYER_ROLL_FACTOR = 0.5  # Percent of steer

PLAYER_AUTOLEVELING_FACTOR = 100
PLAYER_MAX_FLOOR_RAYS = 16  # To find the floor normal

PLAYER_BOOST_FACTOR = 3.0  # Speed increase from boost button
PLAYER_BOOST_SECONDS = 5.0  # Max length of time the player can boost
PLAYER_SHIELD_SECONDS = 0.5  # Max length of time the player can hit walls

PLAYER_SLOW_FACTOR = 0.5
PLAYER_SLOW_SECONDS = 5.0

PLAYER_BOUNCE_FORCE = 1000  # How much the player bounces off walls

PLAYER_BOOST_REGEN_SECONDS = 6.0
PLAYER_SLOW_REGEN_SECONDS = 15.0
PLAYER_SHIELD_REGEN_SECONDS = 60.0

SPEED_UP_TIME = 15  # Seconds to speed up 1 bu

HUD_MESSAGE_TIME = 2.0


ENABLE_INTRO = True
SCORE_FILE = 'score.txt'

ACHIEVEMENTS_LIST = [
    {'type': 'distance', 'data': 200, 'name': '200 meters', 'points': 2000},
    {'type': 'distance', 'data': 500, 'name': '500 meters', 'points': 8000},
    {'type': 'distance', 'data': 1000, 'name': '1000 meters', 'points': 15000},
    {'type': 'distance', 'data': 1500, 'name': '1500 meters', 'points': 30000},
    {'type': 'distance', 'data': 2000, 'name': '1500 meters', 'points': 40000},
    {'type': 'speed', 'data': 200, 'name': 'Jnr. Booster', 'points': 2000},
    {'type': 'speed', 'data': 250, 'name': 'Snr. Booster', 'points': 4000},
    {'type': 'speed', 'data': 300, 'name': 'Speed Freak', 'points': 10000},
    {'type': 'speed', 'data': 400, 'name': 'Speed DEMON', 'points': 20000},
    {'type': 'speed', 'data': 500, 'name': 'Warp God', 'points': 30000},
    {'type': 'pheonix', 'data': (0.3, 0.9), 'name': 'Recovery', 'points': 15000},
    {'type': 'pheonix', 'data': (0.1, 0.8), 'name': 'Pheonix', 'points': 30000},
    {'type': 'pheonix', 'data': (0.01, 0.3), 'name': 'From the Ashes', 'points': 30000}
]
