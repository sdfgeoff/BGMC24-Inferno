#version 120
uniform sampler2D bgl_RenderedTexture;
uniform sampler2D bgl_DepthTexture;

#define STREAKS 1000.0

uniform float streak_length = 3.0;
uniform float zoom_amt = 1.0;
uniform float chromatic_abbr = 0.01;
uniform float fisheye_amt = 2.0;
uniform float streak_center_y = 0.5;
uniform float streak_center_x = 0.5;

uniform float streak_offset = 0.0;

float rand(float num){
    return 0.5 + 0.5 * fract(sin(num * 78.233) * 437585.5453);
}

float getangle(vec2 coords){
    vec2 normalized = coords - vec2(streak_center_x, streak_center_y);
    return float(int(atan(normalized.x / normalized.y) * STREAKS));
}

vec2 noisyzoom(vec2 coords, float zoom, float depth){
    vec2 normalized = coords - vec2(streak_center_x, streak_center_y);
    float dist = length(normalized);

    // Streaks:
    float streaks = rand(getangle(coords)+streak_offset);
    streaks *= pow(dist, 2.0);
    streaks *= streak_length;
    streaks *= depth;
    normalized *= 1.0 - streaks;

    // Zoom:
    normalized *= zoom;

    return normalized + vec2(streak_center_x, streak_center_y);
}


vec2 fisheye(vec2 coords){
    vec2 normalized = coords - vec2(0.5, 0.5);
    float dist = length(normalized);
    return normalized * (dist + fisheye_amt) / (fisheye_amt + 1.0) + vec2(0.5, 0.5);
}


void main(void){
    vec2 orig_coord = gl_TexCoord[0].xy;

    orig_coord = fisheye(orig_coord);

    float depth = clamp(1.0 - pow(texture2D(bgl_DepthTexture, orig_coord).r, 4.0), 0.0, 1.0);

    vec2 texcoord = noisyzoom(orig_coord, zoom_amt, depth);
    gl_FragColor.r = texture2D(bgl_RenderedTexture, texcoord).r + (depth*0.4);

    texcoord = noisyzoom(orig_coord, zoom_amt + chromatic_abbr, depth);
    gl_FragColor.g = texture2D(bgl_RenderedTexture, texcoord).g;

    texcoord = noisyzoom(orig_coord, zoom_amt + chromatic_abbr * 2.0, depth);
    gl_FragColor.b = texture2D(bgl_RenderedTexture, texcoord).b;
}
